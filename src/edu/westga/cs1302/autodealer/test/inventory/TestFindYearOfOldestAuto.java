package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindYearOfOldestAuto {

	@Test
	void testEmptyInventory() {
		Inventory inventory = new Inventory();
		int year = inventory.findYearOfOldestAuto();
		assertEquals(Integer.MAX_VALUE, year);
	}
	
	@Test
	void testOneAutoInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		int year = inventory.findYearOfOldestAuto();
		assertEquals(2008, year);
	}
	
	@Test
	void testMultiAutosOldestFirst() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		int year = inventory.findYearOfOldestAuto();
		assertEquals(2008, year);
	}
	
	@Test
	void testMultiAutosOldestMiddle() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		int year = inventory.findYearOfOldestAuto();
		assertEquals(2008, year);
	}
	
	@Test
	void testMultiAutosOldestLast() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		int year = inventory.findYearOfOldestAuto();
		assertEquals(2008, year);
	}

	
}
