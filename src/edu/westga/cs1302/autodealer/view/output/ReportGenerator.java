package edu.westga.cs1302.autodealer.view.output;

import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		
		if (inventory == null) {
			summary = "No inventory to build report for.";
		} else {
			summary = inventory.toString();
		}

		return summary;
	}

}
